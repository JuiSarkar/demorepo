﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace KOQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue myQueue = new Queue();
            myQueue.Enqueue("Jui");
            myQueue.Enqueue("ECE");
            myQueue.Enqueue(24);
            myQueue.Enqueue(22000.00);
            myQueue.Enqueue('J');

            foreach(var obj in myQueue)
            {
                Console.WriteLine(obj);
            }
            //Remove the 1st item from the queue
            myQueue.Dequeue();
            Console.WriteLine("---------------------------------------------");
            foreach (var obj in myQueue)
            {
                Console.WriteLine(obj);
            }

            //Console.WriteLine("Count value: "+myQueue.Count);
            //Console.WriteLine("Return peek: "+myQueue.Peek()); //It will return not remove
            //Console.WriteLine("Contains: "+myQueue.Contains("Jui")); //Checks whether an item contains or not and returns boolean values
            // myQueue.Clear(); //Removes all the items together

            Console.WriteLine("For Merge Conflict from Desktop");// For Git
            Console.ReadLine();
        }
    }
}
